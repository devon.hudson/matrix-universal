/*============================================================================*/
/*                           This file is part of:                            */
/*                             Matrix Universal                               */
/*============================================================================*/
/* Copyright (c) 2020 Devon Hudson.                                           */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*    http://www.apache.org/licenses/LICENSE-2.0                              */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/*============================================================================*/

#include "MUClient.h"
#include "mu-api.h"

using namespace godot;

void MUClient::_register_methods() {
    register_method("_process", &MUClient::_process);
}

MUClient::MUClient() {
}

MUClient::~MUClient() {
    // add your cleanup here
}

void MUClient::_init() {
    // initialize any variables here
    time_passed = 0.0;
    Initialize();
    RegisterForLogs([=](int logLevel, const char* message)
	{
		String strLog(message);
		LogLevel level = (LogLevel)logLevel;
		switch (level)
		{
            case LogLevel::LOG:
                godot::Godot::print(strLog);
                break;

            case LogLevel::WARN:
                godot::Godot::print_warning(strLog, __FUNCTION__, __FILE__, __LINE__);
                break;

            case LogLevel::ERROR:
                godot::Godot::print_error(strLog, __FUNCTION__, __FILE__, __LINE__);
                break;

            default:
                break;
		}
	});

    int newNum = AddTwoNumbers(2, 5);
}

void MUClient::_process(float delta) {
    time_passed += delta;

    Vector2 new_position = Vector2(10.0 + (10.0 * sin(time_passed * 2.0)), 10.0 + (10.0 * cos(time_passed * 1.5)));

    set_position(new_position);
}
