/*============================================================================*/
/*                           This file is part of:                            */
/*                             Matrix Universal                               */
/*============================================================================*/
/* Copyright (c) 2020 Devon Hudson.                                           */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*    http://www.apache.org/licenses/LICENSE-2.0                              */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/*============================================================================*/

#pragma once

#include <functional>

/*============================================================================*/
/*

Error Codes.
    - Provide consistent error code interface through mu api

Events.

Response?

Inupt Params.

Rooms.

*/
/*============================================================================*/


#if defined(_WIN32) && !defined(_WIN64)
#define STDCALL __stdcall
#else
#define STDCALL
#endif

#if defined(_MSC_VER) // this is defined when compiling with Visual Studio
#define MATRIX_UNIVERSAL_API __declspec(dllexport) // Visual Studio needs annotating exported functions with this
#else
#define MATRIX_UNIVERSAL_API // XCode does not need annotating exported functions, so define is empty
#endif

enum LogLevel
{
    LOG = 0,
    WARN = 1,
    ERROR = 2,
};

#ifdef __cplusplus
extern "C"
{
#endif

#if defined(UNITY)
typedef void(STDCALL *LogCallback)(int, const char*);
#elif defined(UNREAL) || defined(GODOT)
typedef std::function<void(int, const char*)> LogCallback;
#else
typedef std::function<void(int, const char*)> LogCallback;
#endif

MATRIX_UNIVERSAL_API void Initialize();
MATRIX_UNIVERSAL_API void RegisterForLogs(LogCallback callback);

MATRIX_UNIVERSAL_API int AddTwoNumbers(int a, int b);

#ifdef __cplusplus
}
#endif
