/*============================================================================*/
/*                           This file is part of:                            */
/*                             Matrix Universal                               */
/*============================================================================*/
/* Copyright (c) 2020 Devon Hudson.                                           */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*    http://www.apache.org/licenses/LICENSE-2.0                              */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/*============================================================================*/

#ifndef CPPHTTPLIB_OPENSSL_SUPPORT
#define CPPHTTPLIB_OPENSSL_SUPPORT
#endif

#include <iostream>
#include <memory>

#include "httplib.h"
#include "nlohmann/json.hpp"

using json = nlohmann::json;

namespace
{
    const std::string JSON_CONTENT_TYPE = "application/json";

    const std::string LOGIN_TYPE_KEY = "type";
    const std::string LOGIN_FLOWS_KEY = "flows";
    const std::string LOGIN_USERNAME_KEY = "user";
    const std::string LOGIN_PASSWORD_KEY = "password";
    const std::string HOMESERVER_KEY = "m.homeserver";
    const std::string IDENTITY_SERVER_KEY = "m.identity_server";
    const std::string BASE_URL_KEY = "base_url";

    const std::string MATRIX_DEFAULT_LOGIN_FLOW = "m.login.password";
    const std::string MATRIX_ACCESS_TOKEN_KEY = "access_token";
    const std::string MATRIX_WELL_KNOWN = "/.well-known/matrix/client";
    const std::string MATRIX_API_VERSIONS = "/_matrix/client/versions";

    const std::string MATRIX_CLIENT_BASE_PATH = "/_matrix/client/r0";
    const std::string MATRIX_LOGIN_PATH = "/login";
    const std::string MATRIX_SYNC_PATH = "/sync";
}

json handleRepsonse(httplib::Response* response, bool forceJsonParsing=false)
{
    json jsonResponse;

    if(response)
    {
        std::cout << "Response version: " << response->version << std::endl;
        std::cout << "Response status: " << response->status << std::endl;
        std::cout << "Content Type: " << response->get_header_value("Content-Type") << std::endl;

        if(forceJsonParsing || response->get_header_value("Content-Type") == JSON_CONTENT_TYPE)
        {
            jsonResponse = json::parse(response->body, nullptr, false);
            std::cout << "Pretty JSON:\n" << jsonResponse.dump(4) << std::endl;
            std::cout << std::endl;
        }
    }

    return jsonResponse;
}

json httpGet(httplib::SSLClient* cli, std::string uri, httplib::Headers headers, bool forceJsonParsing = false)
{
    std::shared_ptr<httplib::Response> response = cli->Get(uri.c_str(), headers);
    return handleRepsonse(response.get(), forceJsonParsing);
}

json httpGet(httplib::Client* cli, std::string uri, httplib::Headers headers, bool forceJsonParsing = false)
{
    std::shared_ptr<httplib::Response> response = cli->Get(uri.c_str());
    return handleRepsonse(response.get(), forceJsonParsing);
}

json httpPost(httplib::SSLClient* cli, std::string uri, httplib::Headers headers, std::string params, std::string contentType, bool forceJsonParsing = false)
{
    std::shared_ptr<httplib::Response> response = cli->Post(uri.c_str(), params.c_str(), contentType.c_str());
    return handleRepsonse(response.get(), forceJsonParsing);
}

std::vector<std::string> split(const std::string& s, char delimiter)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   while (std::getline(tokenStream, token, delimiter))
   {
      tokens.push_back(token);
   }
   return tokens;
}

bool isLoginFlowAvailable(json loginFlows, std::string flowType)
{
    bool flowExists = false;
    for(auto& item : loginFlows)
    {
        if(item.find(LOGIN_TYPE_KEY) != item.end() &&
           item[LOGIN_TYPE_KEY].get<std::string>() == flowType)
        {
            flowExists = true;
            break;
        }
    }

    return flowExists;
}

bool loginUser(httplib::SSLClient* cli, std::string username, std::string password, std::string& OUT_accessToken)
{
    bool loginSuccess = false;
    httplib::Headers defaultHeaders = {};
    json loginParams = {
        { LOGIN_TYPE_KEY, MATRIX_DEFAULT_LOGIN_FLOW },
        { LOGIN_USERNAME_KEY, username },
        { LOGIN_PASSWORD_KEY, password }
    };
    json result = httpPost(cli, (MATRIX_CLIENT_BASE_PATH + MATRIX_LOGIN_PATH).c_str(), defaultHeaders, loginParams.dump(), JSON_CONTENT_TYPE.c_str());

    if(result.find(MATRIX_ACCESS_TOKEN_KEY) != result.end())
    {
        loginSuccess = true;
        OUT_accessToken = result[MATRIX_ACCESS_TOKEN_KEY].get<std::string>();
    }

    return loginSuccess;
}

int main(int argc, char** argv)
{
    // Handle input arguments
    if(argc != 3)
    {
        std::cout << "Usage: mu-prototype <matrix-id> <password>" << std::endl;
        return 0;
    }

    std::string matrixID = std::string(argv[1]);
    std::string password = std::string(argv[2]);

    std::vector<std::string> splitMatrixID = split(matrixID, ':');

    std::string username = splitMatrixID[0];
    std::string homeserver = splitMatrixID[1];
    std::string homeserverBaseURL;

    httplib::Headers defaultHeaders = {};

    // Create Initial HTTPS Client
    std::unique_ptr<httplib::SSLClient> cli = std::make_unique<httplib::SSLClient>(homeserver.c_str());

    // Obtain the proper homeserver base URL using the well known matrix endpoint
    // TODO : Fully follow Matrix Client-Server Well-Known URI sequence
    json wellKnownResponse = httpGet(cli.get(), MATRIX_WELL_KNOWN.c_str(), defaultHeaders, true);
    if(wellKnownResponse.find(HOMESERVER_KEY) != wellKnownResponse.end() &&
       wellKnownResponse[HOMESERVER_KEY].find(BASE_URL_KEY) != wellKnownResponse[HOMESERVER_KEY].end())
    {
        std::string baseURL = wellKnownResponse[HOMESERVER_KEY][BASE_URL_KEY].get<std::string>();
        std::vector<std::string> strippedURL = split(baseURL, '/');
        if(strippedURL.size() == 3)
        {
            homeserverBaseURL = strippedURL[2];
        }
    }

    if(homeserverBaseURL.empty())
    {
        std::cout << "Failed to obtain homeserver base URL from wellknown matrix endpoint." << std::endl;
        return -1;
    }

    cli.reset(new httplib::SSLClient(homeserverBaseURL.c_str()));

    // Check the matrix api versions supported by the server
    json versionResponse = httpGet(cli.get(), MATRIX_API_VERSIONS.c_str(), defaultHeaders);

    // Get login flows from matrix homeserver
    json loginRespose = httpGet(cli.get(), (MATRIX_CLIENT_BASE_PATH + MATRIX_LOGIN_PATH).c_str(), defaultHeaders);

    // Determine if the http response makes sense
    bool knownFlow = false;
    if(loginRespose.find(LOGIN_FLOWS_KEY) == loginRespose.end())
    {
        std::cout << "Login query failed. Returned:\n" << loginRespose.dump(4) << std::endl;
        return -1;
    }

    json loginFlows = loginRespose[LOGIN_FLOWS_KEY];

    // Exit if there are no compatible login flows
    if(!isLoginFlowAvailable(loginFlows, MATRIX_DEFAULT_LOGIN_FLOW))
    {
        std::cout << "No known login flows detected." << std::endl;

        json knownLoginFlows {
            {
                { LOGIN_TYPE_KEY, MATRIX_DEFAULT_LOGIN_FLOW },
            }
        };
        std::cout << "Known Flows: \n" << knownLoginFlows.dump(4) << std::endl;
        std::cout << "Flows detected: \n" << loginFlows.dump(4) << std::endl;
        return -1;
    }

    // Login user to homeserver
    std::string accessToken;
    if(loginUser(cli.get(), username, password, accessToken))
    {
        httplib::Headers headers = {
            { "Authorization", "Bearer " + accessToken }
        };
        // Sync user account
        httpGet(cli.get(), (MATRIX_CLIENT_BASE_PATH + MATRIX_SYNC_PATH).c_str(), headers);
    }
}
