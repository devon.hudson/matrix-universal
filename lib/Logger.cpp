/*============================================================================*/
/*                           This file is part of:                            */
/*                             Matrix Universal                               */
/*============================================================================*/
/* Copyright (c) 2020 Devon Hudson.                                           */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*    http://www.apache.org/licenses/LICENSE-2.0                              */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/*============================================================================*/

#include "Logger.h"

Logger* Logger::instance = nullptr;

Logger* Logger::GetInstance()
{
	if (instance == NULL)
	{
		instance = new Logger();
	}
	return instance;
}

Logger::Logger() : logCallback(nullptr)
{

}

Logger::~Logger()
{

}

void Logger::RegisterForLogs(LogCallback callback)
{
	logCallback = callback;
}

void Logger::Log(LogLevel level, const char* message)
{
	if (logCallback == nullptr)
	{
		return;
	}

	logCallback(level, message);
}
