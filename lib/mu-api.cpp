/*============================================================================*/
/*                           This file is part of:                            */
/*                             Matrix Universal                               */
/*============================================================================*/
/* Copyright (c) 2020 Devon Hudson.                                           */
/*                                                                            */
/* Licensed under the Apache License, Version 2.0 (the "License");            */
/* you may not use this file except in compliance with the License.           */
/* You may obtain a copy of the License at                                    */
/*                                                                            */
/*    http://www.apache.org/licenses/LICENSE-2.0                              */
/*                                                                            */
/* Unless required by applicable law or agreed to in writing, software        */
/* distributed under the License is distributed on an "AS IS" BASIS,          */
/* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   */
/* See the License for the specific language governing permissions and        */
/* limitations under the License.                                             */
/*============================================================================*/

#include "mu-api.h"
#include "Logger.h"

void Initialize()
{

}

void RegisterForLogs(LogCallback callback)
{
    Logger::GetInstance()->RegisterForLogs(callback);
}

int AddTwoNumbers(int a, int b)
{
    Logger::GetInstance()->Log(LogLevel::LOG, "Hello from the plugin world!");
    Logger::GetInstance()->Log(LogLevel::WARN, "Warning from the plugin world!");
    Logger::GetInstance()->Log(LogLevel::ERROR, "Error from the plugin world!");
    return (a + b);
}
