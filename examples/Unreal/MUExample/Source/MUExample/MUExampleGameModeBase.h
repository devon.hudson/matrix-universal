// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MUExampleGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MUEXAMPLE_API AMUExampleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
