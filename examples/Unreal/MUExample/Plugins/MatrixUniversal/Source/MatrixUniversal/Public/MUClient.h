// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MUClient.generated.h"

UENUM()
enum class ELogLevel : uint8
{
	LOG = 0,
	WARN = 1,
	ERROR = 2,
};

/**
 *
 */
UCLASS()
class MATRIXUNIVERSAL_API UMUClient : public UGameInstanceSubsystem
{
	GENERATED_BODY()

public:
    // Begin USubsystem
    virtual void Initialize(FSubsystemCollectionBase& Collection) override;
    virtual void Deinitialize() override;
    // End USubsystem
};
