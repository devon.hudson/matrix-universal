// Fill out your copyright notice in the Description page of Project Settings.


#include "MUClient.h"
#include "mu-api.h"

void UMUClient::Initialize(FSubsystemCollectionBase& Collection)
{
    UE_LOG(LogTemp, Log, TEXT("Initialize MUClient Subsystem!"));

    RegisterForLogs([=](int logLevel, const char* message)
	{
		FString strLog(message);
		ELogLevel level = (ELogLevel)logLevel;
		switch (level)
		{
            case ELogLevel::LOG:
                UE_LOG(LogTemp, Log, TEXT("%s"), *strLog);
                break;
            case ELogLevel::WARN:
                UE_LOG(LogTemp, Warning, TEXT("%s"), *strLog);
                break;
            case ELogLevel::ERROR:
                UE_LOG(LogTemp, Error, TEXT("%s"), *strLog);
                break;
            default:
                break;
		}
	});

    int result = AddTwoNumbers(3, 8);
}

void UMUClient::Deinitialize()
{

}
