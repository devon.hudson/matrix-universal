// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class MatrixUniversal : ModuleRules
{
	public MatrixUniversal(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;

		// Add any macros that need to be set
        PublicDefinitions.Add("UNREAL");


		if (Target.Platform == UnrealTargetPlatform.Win64)
		{
			// Add the import library
			PublicAdditionalLibraries.Add(ModuleDirectory + "/lib/mu-unreal.lib");
        }
        else
        {
			// Add the import library
			PublicAdditionalLibraries.Add(ModuleDirectory + "/lib/mu-unreal.a");
        }


		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
			}
			);


		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
				"../../../../../../include/"
			}
			);


		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"CoreUObject",
				"Engine",
				"Projects"
				// ... add other public dependencies that you statically link with here ...
			}
			);


		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				// ... add private dependencies that you statically link with here ...
			}
			);


		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
