﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class MatrixClient : MonoBehaviour
{
    const string c_strDLLName = "mu-unity";
    [DllImport(c_strDLLName, CallingConvention = CallingConvention.Cdecl)]
    private static extern int AddTwoNumbers(int a, int b);
    [DllImport(c_strDLLName, CallingConvention = CallingConvention.Cdecl)]
    private static extern int RegisterForLogs(LoggingCallback callback);

    public delegate void LoggingCallback(int logLevel, string message);
    LoggingCallback onLogReceived;

    enum LogLevel
    {
        LOG = 0,
        WARN = 1,
        ERROR = 2,
    };

    // Start is called before the first frame update
    void Start()
    {
        onLogReceived += OnLogReceived;
        RegisterForLogs(onLogReceived);

        int num1 = 10;
        int num2 = 7;
        int sum = AddTwoNumbers(num1, num2);

        Debug.Log(string.Format("The sum of {0} and {1} is {2}", num1, num2, sum));
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnLogReceived(int logLevel, string message)
    {
        LogLevel eLogLevel = (LogLevel)logLevel;
        switch(eLogLevel)
        {
            case LogLevel.LOG:
                Debug.Log(message);
                break;
            case LogLevel.WARN:
                Debug.LogWarning(message);
                break;
            case LogLevel.ERROR:
                Debug.LogError(message);
                break;
        }
    }
}
