# Building Plugins

## Dependencies

The following must be available in order to build Matrix Universal.

- openssl <https://github.com/openssl/openssl>

## Preparing the build

```
mkdir build
cd build
cmake ..
```

### Windows

```
cmake --build . --config Release
cmake --install .
```

### Linux

```
make
make install
```
